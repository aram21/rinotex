//chekbox real-legal code
var count = 0;
$(".span1, .span3, .span2").click(function(){
	count++;
	if(count%2 !== 0){
    	$(".span1").css("margin-left","0");
        $(".title1").css("display","none");
        $(".titlehide").css("display","block");
        $(".content1").css("display","none");
        $(".contenthide").css("display","block");
	}else {
        $(".span1").css("margin-left","-80px");
        $(".title1").css("display","block");
        $(".titlehide").css("display","none");
        $(".content1").css("display","block");
        $(".contenthide").css("display","none");
	}
});

//chekbox male-female code
var countt = 0;
$(".span11, .span33, .span22").click(function(){
    countt++;
    if(countt%2 !== 0){
        $(".span11").css("margin-left","0");
    }else {
        $(".span11").css("margin-left","-80px");
    }
});

//hide menu code
$(function(){
  $(window).scroll(function(){
    var aTop = $('.inner-banner').height();
    // var bTop = $('.menubar').height();
    if($(this).scrollTop() >= aTop) {
        $(".hidemenu").css("margin-top","0");
        $(".menubar").css("height","0");
    }else {
        $(".hidemenu").css("margin-top","-100%");
        $(".menubar").css("height","59px");
    }
  });
});

//input-file code
$('.my1').change(function() {
    if ($(this).val() != '') {
        $(".fileinput1").html($(this)[0].files[0].name);
        $(".fileinput1").css("width","583px");
        $(".remove1").css("display","block");
    }
});
$('.my2').change(function() {
    if ($(this).val() != '') { 
        $(".fileinput2").html($(this)[0].files[0].name);
        $(".fileinput2").css("width","583px");
        $(".remove2").css("display","block");
    }
});
$('.my3').change(function() {
    if ($(this).val() != '') { 
        $(".fileinput3").html($(this)[0].files[0].name);
        $(".fileinput3").css("width","583px");
        $(".remove3").css("display","block");
    }
});
$('.remove1').click(function() {
    $(".my1").val('');
    $(".fileinput1").empty();
    $(".fileinput1").css("width","667px");
    $(".remove1").css("display","none");  
});
$('.remove2').click(function() {
    $(".my2").val('');
    $(".fileinput2").empty();
    $(".fileinput2").css("width","667px");
    $(".remove2").css("display","none");  
});
$('.remove3').click(function() {
    $(".my3").val('');
    $(".fileinput3").empty();
    $(".fileinput3").css("width","667px");
    $(".remove3").css("display","none");  
});

//form validation
function valid() {
    var val = true;
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var phoneno = /^0(41|43|44|55|91|93|94|95|96|97|98|99)[0-9]{6}$/;

    if($("#checkbox").val() == 0) { //real-user
        if(document.getElementById('firstname').value == 0) {
            document.getElementById('nameMark1').style.color = "red";
            val = false;
        }else {
            document.getElementById('nameMark1').style.color = "green";
            $("#nameMark1").removeClass("fa-exclamation");
            $("#nameMark1").addClass("fa-check");
        }

        if(document.getElementById('lastname').value == 0) {
            document.getElementById('nameMark2').style.color = "red";
            val = false;
        }else {
            document.getElementById('nameMark2').style.color = "green";
            $("#nameMark2").removeClass("fa-exclamation");
            $("#nameMark2").addClass("fa-check");
        }

        if(document.getElementById('ID').value == 0) {
            document.getElementById('nameMark3').style.color = "red";
            val = false;
        }else {
            document.getElementById('nameMark3').style.color = "green";
            $("#nameMark3").removeClass("fa-exclamation");
            $("#nameMark3").addClass("fa-check");
        }

        if(document.getElementById('academic').value == 0) {
            document.getElementById('nameMark4').style.color = "red";
            val = false;
        }else {
            document.getElementById('nameMark4').style.color = "green";
            $("#nameMark4").removeClass("fa-exclamation");
            $("#nameMark4").addClass("fa-check");
        }

        if(document.getElementById('job').value == 0) {
            document.getElementById('nameMark5').style.color = "red";
            val = false;
        }else {
            document.getElementById('nameMark5').style.color = "green";
            $("#nameMark5").removeClass("fa-exclamation");
            $("#nameMark5").addClass("fa-check");
        }

        if(document.getElementById('CareerField').value == 0) {
            document.getElementById('nameMark6').style.color = "red";
            val = false;
        }else {
            document.getElementById('nameMark6').style.color = "green";
            $("#nameMark6").removeClass("fa-exclamation");
            $("#nameMark6").addClass("fa-check");
        }

        if(document.getElementById('cv').value == 0) {
            document.getElementById('nameMark7').style.color = "red";
            val = false;
        }else {
            document.getElementById('nameMark7').style.color = "green";
            $("#nameMark7").removeClass("fa-exclamation");
            $("#nameMark7").addClass("fa-check");
        }
    }else{ //legal-user
        if(document.getElementById('Company').value == 0) {
            document.getElementById('nameMark15').style.color = "red";
            val = false;
        }else {
            document.getElementById('nameMark15').style.color = "green";
            $("#nameMark15").removeClass("fa-exclamation");
            $("#nameMark15").addClass("fa-check");
        }

        if(document.getElementById('Director').value == 0) {
            document.getElementById('nameMark16').style.color = "red";
            val = false;
        }else {
            document.getElementById('nameMark16').style.color = "green";
            $("#nameMark16").removeClass("fa-exclamation");
            $("#nameMark16").addClass("fa-check");
        }

        if(document.getElementById('field').value == 0) {
            document.getElementById('nameMark17').style.color = "red";
            val = false;
        }else {
            document.getElementById('nameMark17').style.color = "green";
            $("#nameMark17").removeClass("fa-exclamation");
            $("#nameMark17").addClass("fa-check");
        }

        if(document.getElementById('crn').value == 0) {
            document.getElementById('nameMark18').style.color = "red";
            val = false;
        }else {
            document.getElementById('nameMark18').style.color = "green";
            $("#nameMark18").removeClass("fa-exclamation");
            $("#nameMark18").addClass("fa-check");
        }

        if(document.getElementById('Posta').value == 0) {
            document.getElementById('nameMark19').style.color = "red";
            val = false;
        }else {
            document.getElementById('nameMark19').style.color = "green";
            $("#nameMark19").removeClass("fa-exclamation");
            $("#nameMark19").addClass("fa-check");
        }
    }

    //account-info validate
    if(!re.test(document.getElementById('email').value)) {
        document.getElementById('nameMark8').style.color = "red";
        val = false;
    }else {
        document.getElementById('nameMark8').style.color = "green";
        $("#nameMark8").removeClass("fa-exclamation");
        $("#nameMark8").addClass("fa-check");
    }

    if(document.getElementById('emailcode').value <= 6) {
        document.getElementById('nameMark9').style.color = "red";
        val = false;
    }else {
        document.getElementById('nameMark9').style.color = "green";
        $("#nameMark9").removeClass("fa-exclamation");
        $("#nameMark9").addClass("fa-check");
    }

    if(document.getElementById('pass').value.length < 6) {
        document.getElementById('nameMark10').style.color = "red";
        val = false;
    } else {
        document.getElementById('nameMark10').style.color = "green";
        $("#nameMark10").removeClass("fa-exclamation");
        $("#nameMark10").addClass("fa-check");
        if(document.getElementById('pass').value.length != document.getElementById('confpass').value.length) {
            document.getElementById('nameMark11').style.color = "red";
            val = false;
        } else {
            document.getElementById('nameMark11').style.color = "green";
            $("#nameMark11").removeClass("fa-exclamation");
            $("#nameMark11").addClass("fa-check");
        }
    }
    
    if(document.getElementById('Country').value == 0) {
        document.getElementById('nameMark12').style.color = "red";
        val = false;
    }else {
        document.getElementById('nameMark12').style.color = "green";
        $("#nameMark12").removeClass("fa-exclamation");
        $("#nameMark12").addClass("fa-check");
    }

    if(document.getElementById('city').value == 0) {
        document.getElementById('nameMark13').style.color = "red";
        val = false;
    }else {
        document.getElementById('nameMark13').style.color = "green";
        $("#nameMark13").removeClass("fa-exclamation");
        $("#nameMark13").addClass("fa-check");
    }

    if(!phoneno.test(document.getElementById('phone').value)) {
        document.getElementById('nameMark14').style.color = "red";
        val = false;
    }else {
        document.getElementById('nameMark14').style.color = "green";
        $("#nameMark14").removeClass("fa-exclamation");
        $("#nameMark14").addClass("fa-check");
    }

    return val;

}

$("#send").click(function() {

    var email = $("#email").val();
    if(ValidateEmail(email)){
        var email = $("#email").serialize();
        $.ajax({
            type: "POST", //Метод отправки
            url: "mail.php", //путь до php фаила отправителя
            data: email,
            success: function() {
                    //код в этом блоке выполняется при успешной отправке сообщения
                alert("Your message has send. Please check your email.");
                document.getElementById("emailcode").disabled = false;
                document.getElementById("codebutton").disabled = false;
            }
        });

    }else{
        alert('error email');
    }
   
});    

function ValidateEmail(mail)
    {
       if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
       {
           return (true)
       }

       return (false)
    }


$("#codebutton").click(function() {
    
    var emailcode = $("#emailcode").val();
    if(emailcode.length <= 6){
        var emailcode = $("#emailcode").serialize();
        $.ajax({
            type: "POST",
            url: "mail.php",
            data: emailcode,
            success: function(msg) {
                
                if(msg == 'yes'){
                    alert('Your email verificated.');
                    $("#send").css("display","none");
                    $(".labl").css("display","none");
                    $("#emailcode").css("display","none");
                    $("#codebutton").css("display","none");
                    $("#nameMark8").css("color","green");
                    $("#nameMark8").removeClass("fa-exclamation");
                    $("#nameMark8").addClass("fa-check");
                }else if(msg == 'no') {
                    alert('Your email is NOT verificated.');
                }
           
            }
        });
    }

});
